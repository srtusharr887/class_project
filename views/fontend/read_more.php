<?php
include('../../vendor/autoload.php');
use App\fontend\fontend;
$id=$_GET['id'];
$fnt=new fontend();
$result=$fnt->selectAllById($id);
?>
<!DOCTYPE html>
<!--[if lt IE 8 ]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 8)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

    <!--- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
   ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <!-- CSS
     ================================================== -->
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="../../asset/fontend/css/default.css">
    <link rel="stylesheet" href="../../asset/fontend/css/layout.css">
    <link rel="stylesheet" href="../../asset/fontend/css/media-queries.css">


    <!-- Script
    ================================================== -->
    <script src="../../asset/fontend/js/modernizr.js"></script>

    <!-- Favicons
     ================================================== -->
    <link rel="../../asset/fontend/shortcut icon" href="favicon.png" >

</head>

<body>

<!-- Header
================================================== -->
<header id="top">

    <div class="row">

        <div class="header-content twelve columns">

            <h1 id="logo-text"><a href="index.html" title="">Here Is Question</a></h1>
            <p id="intro">Put your Question</p>

        </div>

    </div>

    <nav id="nav-wrap">

        <a class="mobile-btn" href="#nav-wrap" title="Show navigation">Show Menu</a>
        <a class="mobile-btn" href="#" title="Hide navigation">Hide Menu</a>

        <div class="row">

            <ul id="nav" class="nav">
                <li class="current"><a href="home.php">Home</a></li>
            </ul>

            <!-- end #nav -->

        </div>

    </nav> <!-- end #nav-wrap -->

</header> <!-- Header End -->

<!-- Content
================================================== -->
<div id="content-wrap">

    <div class="row">
        <div id="main" class="eight columns">
            <?php
            foreach ($result as $row){
                ?>
                <article class="entry">
                    <header class="entry-header">
                        <h2 class="entry-title">
                            <a href="" title=""><?php echo $row['author_question'];?></a>
                        </h2>
                        <div class="entry-meta">
                            <ul>
                                <li>July 12, 2014</li>
                                <span class="meta-sep">&bull;</span>
                                <li><a href="#" title="" rel="category tag">Ghost</a></li>
                                <span class="meta-sep">&bull;<?php echo $row['author_name'];?></span>
                                <li></li>
                            </ul>
                        </div>
                    </header>
                    <div class="entry-content">
                        <p><?php echo $row['description'];?></p>
                    </div>
                </article> <!-- end entry -->
            <?php } ?>


        </div> <!-- end main -->

        <div id="sidebar" class="four columns">

            <div class="widget widget_search">
                <h3>Search</h3>
                <form action="#">

                    <input type="text" value="Search here..." onblur="if(this.value == '') { this.value = 'Search here...'; }" onfocus="if (this.value == 'Search here...') { this.value = ''; }" class="text-search">
                    <input type="submit" value="" class="submit-search">

                </form>
            </div>

            <div class="widget widget_categories group">
                <h2>Basic List Group</h2>
                <div class="container" >

                    <ul class="list-group  col-md-4">
                        <li class="list-group-item">
                            <h3>First item</h3>
                            <hr>
                            <p>this is a list</p>
                        </li>
                        <li class="list-group-item">
                            <h3>First item</h3>
                            <hr>
                            <p>this is a list</p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="widget widget_text group">
                <h3>Widget Text.</h3>

                <p>Lorem ipsum Ullamco commodo laboris sit dolore commodo aliquip incididunt fugiat esse dolor aute fugiat minim eiusmod do velit labore fugiat officia ad sit culpa labore in consectetur sint cillum sint consectetur voluptate adipisicing Duis irure magna ut sit amet reprehenderit.</p>

            </div>

            <div class="widget widget_tags">
                <h3>Post Tags.</h3>

                <div class="tagcloud group">
                    <a href="#">Corporate
                        <p>hi</p></a>

                </div>

            </div>

            <div class="widget widget_popular">
                <h3>Popular Post.</h3>

                <ul class="link-list">
                    <li><a href="#">Sint cillum consectetur voluptate.</a></li>
                    <li><a href="#">Lorem ipsum Ullamco commodo.</a></li>
                    <li><a href="#">Fugiat minim eiusmod do.</a></li>
                </ul>

            </div>

        </div> <!-- end sidebar -->

    </div> <!-- end row -->

</div> <!-- end content-wrap -->


<!-- Footer
================================================== -->
<footer>

    <div class="row">

        <div class="twelve columns">
            <ul class="social-links">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-github-square"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                <li><a href="#"><i class="fa fa-skype"></i></a></li>
            </ul>
        </div>

        <div class="six columns info">

            <h3>About Keep It Simple</h3>

            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem
                nibh id elit.
            </p>

            <p>Lorem ipsum Sed nulla deserunt voluptate elit occaecat culpa cupidatat sit irure sint
                sint incididunt cupidatat esse in Ut sed commodo tempor consequat culpa fugiat incididunt.</p>

        </div>

        <div class="four columns">

            <h3>Photostream</h3>

            <ul class="photostream group">
                <li><a href="#"><img alt="thumbnail" src="../../asset/fontend/images/thumb.jpg"></a></li>
                <li><a href="#"><img alt="thumbnail" src="../../asset/fontend/images/thumb.jpg"></a></li>
                <li><a href="#"><img alt="thumbnail" src="../../asset/fontend/images/thumb.jpg"></a></li>
                <li><a href="#"><img alt="thumbnail" src="../../asset/fontend/images/thumb.jpg"></a></li>
                <li><a href="#"><img alt="thumbnail" src="../../asset/fontend/images/thumb.jpg"></a></li>
                <li><a href="#"><img alt="thumbnail" src="../../asset/fontend/images/thumb.jpg"></a></li>
                <li><a href="#"><img alt="thumbnail" src="../../asset/fontend/images/thumb.jpg"></a></li>
                <li><a href="#"><img alt="thumbnail" src="../../asset/fontend/images/thumb.jpg"></a></li>
            </ul>

        </div>

        <div class="two columns">
            <h3 class="social">Navigate</h3>

            <ul class="navigate group">
                <li><a href="#">Home</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Demo</a></li>
                <li><a href="#">Archives</a></li>
                <li><a href="#">About</a></li>
            </ul>
        </div>

        <p class="copyright">&copy; Copyright 2014 Keep It Simple. &nbsp; Design by <a title="Styleshout" href="http://www.styleshout.com/">Styleshout</a>.</p>

    </div> <!-- End row -->

    <div id="go-top"><a class="smoothscroll" title="Back to Top" href="#top"><i class="fa fa-chevron-up"></i></a></div>

</footer> <!-- End Footer-->


<!-- Java Script
================================================== -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../asset/fontend/js/jquery-1.10.2.min.js"><\/script>')</script>
<script type="text/javascript" src="../../asset/fontend/js/jquery-migrate-1.2.1.min.js"></script>
<script src="../../asset/fontend/js/main.js"></script>

</body>

</html>