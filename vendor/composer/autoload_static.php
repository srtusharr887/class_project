<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitfa5e8db0a7cca752567235dae6da9dd9
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/class',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitfa5e8db0a7cca752567235dae6da9dd9::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitfa5e8db0a7cca752567235dae6da9dd9::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
